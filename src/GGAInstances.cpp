//     This file is part of DGGA.
//     Copyright (C) 2015 Kevin Tierney and Josep Pon
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.


                
#include <fstream>

#include <boost/algorithm/string/trim.hpp>

#include "GGAExceptions.hpp"
#include "GGAInstances.hpp"
#include "OutputLog.hpp"

/**
 * @brief Creates a new empty instance of GGAInstances.
 */
GGAInstances::GGAInstances()
    : m_instances()
    , m_instances_index()
    , m_clusters()
{ }

/**
 * @brief Creates a new instance of GGAInstances as a copy of the given one.
 * @param orig
 */
GGAInstances::GGAInstances(const GGAInstances& orig)
    : m_instances(orig.m_instances)
    , m_instances_index(orig.m_instances_index)
    , m_clusters(orig.m_clusters)
{ }

/**
 * Virtual destructor.
 */
GGAInstances::~GGAInstances()
{ }

/**
 * @brief Loads instances from an instance-seed file.
 */
void GGAInstances::loadInstancesFile(const std::string& file_path)
{
    std::ifstream ifs(file_path.c_str());

    if (ifs.is_open()) {
        std::string line;

        for (int line_num = 1; std::getline(ifs, line); ++line_num) {
            boost::algorithm::trim(line);

            try {
                GGAInstance instance = GGAInstance::fromString(line);
                m_instances_index[instance.getInstance()] = m_instances.size();
                m_instances.push_back(instance);
            } catch (const GGAMalformedInstanceException& e) {
                throw GGAMalformedInstanceException(e.message(), file_path,
                                                 line_num);
            }
        }
        ifs.close();

    } else {
        throw GGAFileNotFoundException(file_path);
    }
}

/**
 * @brief Loads instance clusters from a clusters file.
 */
void GGAInstances::loadInstanceClustersFile(const std::string& file_path) 
{
    std::ifstream ifs(file_path.c_str());
    
    if (ifs.is_open()) {
        std::string line;
        std::vector<unsigned> cluster_instances_index;
        
        LOG_VERY_VERBOSE("==== Parsing clusters ====")
        for (int line_num = 1; std::getline(ifs, line); ++line_num) {
            boost::algorithm::trim(line);
            
            if (!line.empty()) {
                if (m_instances_index.count(line)) {
                    LOG_DEBUG(line << " Index: " << m_instances_index[line]);
                    cluster_instances_index.push_back(m_instances_index[line]);
                } else {
                    throw GGAException(
                            "[GGAInstances::loadInstanceClustersFile] Non"
                            " existen instance. Check the instances file.");
                }
            } else {
                if (!cluster_instances_index.empty()) {
                    m_clusters.push_back(cluster_instances_index);
                    LOG_VERY_VERBOSE("---- Parsed cluster with: " 
                                     << cluster_instances_index.size()
                                     << " instances ----");
                    LOG_DEBUG("---- New cluster ----");
                }
                cluster_instances_index.clear();
            }
        }
        LOG_VERY_VERBOSE("==== Parsing clusters ====");
        
        ifs.close();
    } else {
        throw GGAFileNotFoundException(file_path);
    }
}
