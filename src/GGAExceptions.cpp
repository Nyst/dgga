//     This file is part of DGGA.
//     Copyright (C) 2015 Kevin Tierney and Josep Pon
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.


                
#include <cerrno>
#include <cstring>
#include <sstream>

#include "GGAExceptions.hpp"
#include "OutputLog.hpp"

//
// === GGAException ===
//

/**
 * @brief Initializes an instance of GGAException with an empty error message.
 */
GGAException::GGAException()
    : m_message()
{ }


/**
 * @brief Initializes an instance of GGAException as a copy of another one.
 */
GGAException::GGAException(const GGAException& other)
    : m_message(other.m_message)
{ }


/**
 * @brief Initializes an instance of GGAException with the given error message.
 */
GGAException::GGAException(const std::string& err_msg)
    : m_message(err_msg)
{ }


/**
 *
 */
GGAException::~GGAException() throw()
{ }


/**
 * @return The c-string representation of the excpetion's message.
 */
const char* GGAException::what() const throw()
{ return m_message.c_str(); }


/**
 * @return The exception's error message.
 */
const std::string& GGAException::message() const
{ return m_message; }


/**
 * @brief Sets the exception's error message.
 */
void GGAException::message(const std::string& message)
{ m_message = message; }


//
// === GGAMalformedInstanceException ===
//

GGAMalformedInstanceException::GGAMalformedInstanceException(
        const std::string& inst)
    : GGAException(inst)
{ }


GGAMalformedInstanceException::GGAMalformedInstanceException(
        const std::string& inst,
        const std::string& file,
        int line) 
    : GGAException()
{
    std::stringstream ss;

    ss << "Malformed Instance";
    if (line > -1)
        ss << " at line " << line;
    if (!file.empty())
        ss << " in " << file;
    ss << ": " << inst << std::endl; 
    ss << "Proper formatting is: [seed] <instance> [extra parameter 1 ... n]";

    message(ss.str());
}

//
// === GGAFileNotFoundException ===
//

GGAFileNotFoundException::GGAFileNotFoundException(const std::string& file) 
        : GGAException("File not found: " + file)
{ }


//
// === GGAParameterException ===
//

GGAParameterException::GGAParameterException(const std::string& prob)
    : GGAException(prob)
{ }


//
// === GGAOptionsException ===
//

GGAOptionsException::GGAOptionsException(const std::string& prob) 
    : GGAException(prob)
{ }


//
// === GGAPopulationException ===
//

GGAPopulationException::GGAPopulationException(const std::string& prob) 
    : GGAException("Population exception: " + prob) 
{ }


//
// === GGAXMLParseException ===
//

GGAXMLParseException::GGAXMLParseException(const std::string& msg)
    : GGAException(msg)
{ }


//
// === GGACommandException ===
//

GGACommandException::GGACommandException(const std::string& msg)
    : GGAException(msg)
{ }


//
// === GGAScenarioFileException ===
//

GGAScenarioFileException::GGAScenarioFileException(const std::string& msg)
  : GGAException(msg)
{ }


//
// === GGAInterruptedException ===
//

GGAInterruptedException::GGAInterruptedException(const std::string& msg)
  : GGAException(msg)
{ }


//
// === GGANullPointerException ===
//

GGANullPointerException::GGANullPointerException(const std::string& msg)
    : GGAException(msg)
{ }
