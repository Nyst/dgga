//     This file is part of DGGA.
//     Copyright (C) 2015 Kevin Tierney and Josep Pon
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.


                

#include <stdexcept>

#include <boost/lexical_cast.hpp>
#include <boost/regex.hpp>

#include "GGAValue.hpp"
#include "OutputLog.hpp"


//==============================================================================
// GGAValue public static methods

/**
 * Converts the string into the appropriate GGAValue, forcing the type to
 * be forceType if it is set to anything other than UNKNOWN.
 */
GGAValue GGAValue::getValueFromStr(const std::string& str, ValueType forceType)
{
    // TODO bool support
    boost::regex dblP("^-?\\d+\\.\\d+((e|E)(\\+?|-)\\d+)?$");
    boost::regex intP("^-?\\d+$");
    

    if (forceType == LONG 
            || (boost::regex_match(str, intP) && forceType == UNKNOWN)) {
        return GGAValue(boost::lexical_cast<long>(str));
    } else if(forceType == DOUBLE 
            || (boost::regex_match(str, dblP) && forceType == UNKNOWN)) {
        return GGAValue(boost::lexical_cast<double>(str));
    } else {
        return GGAValue(str);
    }
}


//==============================================================================
// GGAValue public methods

/**
 *
 */
GGAValue::GGAValue()
    : m_type(UNKNOWN)
    , m_boolValue()
    , m_longValue()
    , m_dblValue()
    , m_strValue()
{ }


/**
 *
 */
GGAValue::GGAValue(const GGAValue& cpy)
    : m_type(cpy.m_type)
    , m_boolValue(m_type == BOOL ? new bool(*cpy.m_boolValue) : 0)
    , m_longValue(m_type == LONG ? new long(*cpy.m_longValue) : 0)
    , m_dblValue(m_type == DOUBLE ? new double(*cpy.m_dblValue) : 0)
    , m_strValue(m_type == STRING ? new std::string(*cpy.m_strValue) : 0)
    
{ }

/**
 *
 */
GGAValue::GGAValue(bool val) 
    : m_type(BOOL)
    , m_boolValue(new bool(val))
    , m_longValue()
    , m_dblValue()
    , m_strValue() 
{ }

/**
 *
 */
GGAValue::GGAValue(long val)
    : m_type(LONG)
    , m_boolValue()
    , m_longValue(new long(val))
    , m_dblValue()
    , m_strValue()
{ }


/**
 *
 */
GGAValue::GGAValue(double val) 
    : m_type(DOUBLE)
    , m_boolValue()
    , m_longValue()
    , m_dblValue(new double(val))
    , m_strValue()    
{ }

/**
 *
 */
GGAValue::GGAValue(const std::string& val)
    : m_type(STRING)
    , m_boolValue()
    , m_longValue()
    , m_dblValue()
    , m_strValue(new std::string(val))
{ }


/**
 *
 */
GGAValue::~GGAValue() 
{ }


/**
 *
 */
void GGAValue::setValue(bool val) 
{
    resetValues();

    m_boolValue.reset(new bool(val));
    m_type = BOOL;
}


/**
 *
 */
void GGAValue::setValue(long val)
{
    resetValues();

    m_longValue.reset(new long(val));
    m_type = LONG;
}



/**
 *
 */
void GGAValue::setValue(double val) 
{
    resetValues();
    
    m_dblValue.reset(new double(val));
    m_type = DOUBLE;
}


/**
 *
 */
void GGAValue::setValue(const std::string& val) 
{
    resetValues();

    m_strValue.reset(new std::string(val));
    m_type = STRING;
}


/**
 *
 */
bool GGAValue::getBool() const
{
    if(isBool())
        return *m_boolValue;
    else
        throw std::domain_error("Attempted to access a bool value in GGAValue"
            " that is not storing a bool value.");
}


/**
 *
 */
long GGAValue::getLong() const
{
    if(isLong())
        return *m_longValue;
    else
        throw std::domain_error("Attempted to access a long value in GGAValue"
            " that is not storing a long value.");
}


/**
 *
 */
double GGAValue::getDouble() const
{
    if(isDouble())
        return *m_dblValue;
    else
        throw std::domain_error("Attempted to access a double value in GGAValue"
            " that is not storing a double value.");
}


/**
 *
 */
const std::string& GGAValue::getString() const
{
    if(isString())
        return *m_strValue;
    else
        throw std::domain_error("Attempted to access a string value in GGAValue"
            " that is not storing a string value.");
}


/**
 *
 */
std::string GGAValue::toString() const {
    std::stringstream ss;
    if(isLong() && m_longValue.get()) {
        ss << *m_longValue;
    } else if(isBool() && m_boolValue.get()) {
        ss << *m_boolValue;
    } else if(isDouble() && m_dblValue.get()) {
        ss << *m_dblValue;
    } else if(isString() && m_strValue.get()) {
        ss << *m_strValue;
    }

    return ss.str();
}


/**
 *
 */
GGAValue& GGAValue::operator=(const GGAValue& cpy)
{
    resetValues();
    m_type = cpy.m_type;

    switch (cpy.m_type) {
        case BOOL:
            m_boolValue.reset(new bool(*cpy.m_boolValue));
            break;
        case LONG:
            m_longValue.reset(new long(*cpy.m_longValue));
            break;
        case DOUBLE:
            m_dblValue.reset(new double(*cpy.m_dblValue));
            break;
        case STRING:
            m_strValue.reset(new std::string(*cpy.m_strValue));
            break;
        default:
            break;
    }

    return *this;
}


/**
 *
 */
bool GGAValue::operator==(const GGAValue& other) const
{
    if(isLong() && other.isLong())
        return getLong() == other.getLong();
    if(isBool() && other.isBool())
        return getBool() == other.getBool();
    if(isDouble() && other.isDouble())
        return getDouble() == other.getDouble();
    if(isString() && other.isString())
        return getString() == other.getString();

    return false;
}


//==============================================================================
// GGAValue private methods

/**
 *
 */
void GGAValue::resetValues()
{
    m_type = UNKNOWN;

    m_longValue.reset();
    m_boolValue.reset();
    m_dblValue.reset();
    m_strValue.reset();
}

//==============================================================================
// GGAValue stream operators

std::ostream& operator<<(std::ostream& output, const GGAValue& val) {
    output << val.toString();
    return output;
}
