//     This file is part of DGGA.
//     Copyright (C) 2015 Kevin Tierney and Josep Pon
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.


                

#include <cstdlib>

#include "GGAOptions.hpp"
#include "OutputLog.hpp"
#include "GGARandEngine.hpp"

bool GGARandEngine::hasSeed = false;
boost::minstd_rand0 GGARandEngine::eng;


boost::minstd_rand0& GGARandEngine::getEngine()
{
    if(!hasSeed) {
        const GGAOptions& opts = GGAOptions::instance();
        eng.seed(opts.seed);
        std::srand(opts.seed); // Just be sure...
        hasSeed = true;
		LOG_DEBUG("Setting GGARandEngine seed: " << opts.seed);
    }

    return eng;
}


/**
 * Returns uniformly distributed random int
 * rangeStart <= rangeEnd
 * x \in [rangeStart, rangeEnd]
 */
int GGARandEngine::randInt(int rangeStart, int rangeEnd)
{
    boost::uniform_int<int> dist(rangeStart, rangeEnd);
    boost::variate_generator< boost::minstd_rand0&, boost::uniform_int<int> > 
        r(getEngine(), dist);
    int tmp = r();
    return tmp;
}


/**
 * Returns uniformly distributed random long
 * rangeStart <= rangeEnd
 * x \in [rangeStart, rangeEnd]
 */
long GGARandEngine::randLong(long rangeStart, long rangeEnd)
{
    boost::uniform_int<long> dist(rangeStart, rangeEnd);
    boost::variate_generator< boost::minstd_rand0&, boost::uniform_int<long> > 
        r(getEngine(), dist);
    long tmp = r();
    return tmp;
}


/**
 * Returns a uniformly distributed random double
 * rangeStart <= rangeEnd
 * x \in [rangeStart, rangeEnd]
 */
double GGARandEngine::randDouble(double rangeStart, double rangeEnd)
{
    boost::uniform_real<> dist(rangeStart, rangeEnd);
    boost::variate_generator<boost::minstd_rand0&, boost::uniform_real<> >
        r(getEngine(), dist);

    /*LOG("DOUBLE: " << r());
    LOG("DOUBLE: " << r());*/

    return r();
}


/**
 *
 */
bool GGARandEngine::coinFlip()
{
    return randDouble(0.0, 1.0) < 0.5;
}


/**
 *
 */
int GGARandEngine::randGaussianInt(int mu, int rangeStart, int rangeEnd)
{
    double randVal = randGaussianDouble(static_cast<double>(mu),
        static_cast<double>(rangeStart), static_cast<double>(rangeEnd));
    return static_cast<int>(randVal);
}


/**
 *
 */
long GGARandEngine::randGaussianLong(long mu, long rangeStart, long rangeEnd)
{
    double randVal = randGaussianDouble(static_cast<double>(mu),
        static_cast<double>(rangeStart), static_cast<double>(rangeEnd));
    return static_cast<long>(randVal);
}


/**
 *
 */
double GGARandEngine::randGaussianDouble(double mu, double rangeStart,
                                         double rangeEnd)
{
    const GGAOptions& opts = GGAOptions::instance();
    
    double sigma = (rangeEnd - rangeStart) * opts.sigma_pct;
    boost::normal_distribution<double> dist(mu, sigma);
    boost::variate_generator<
                    boost::minstd_rand0&,
                    boost::normal_distribution<double> > r(getEngine(), dist);
    double rval = r();

    int preventInf = 0;
    while ((rval < rangeStart || rval > rangeEnd) && preventInf < 400) {
        rval = r();
        preventInf += 1;
    }

    if(preventInf >= 400) {
        // TODO Exception??
        LOG_ERROR("Unable to randomly generate a value in the given range from"
                  " a Gaussian distribution after 400 tries. This should not"
                  " happen.");
        rval = mu;
    }
    
    return rval;
}
