//     This file is part of DGGA.
//     Copyright (C) 2015 Kevin Tierney and Josep Pon
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.


                
/*
 * Class: RandEngine
 * Author: Kevin Tierney
 * Purpose: Returns random numbers using tr1/random
 *
 * Modified: Josep Pon Farreny
 * Changed tr1 random to boost random, due to missing tr1 files in OS X.
 */

#include <boost/random.hpp>
#include <ctime>

#include "ggatypedefs.hpp"

#ifndef _GGA_RAND_ENGINE_HPP_
#define _GGA_RAND_ENGINE_HPP_

class GGARandEngine {
    private:
        // Public static variables
        //
        static bool hasSeed;
        static boost::minstd_rand0 eng;

        // construct
        GGARandEngine() {}

    public:
        static boost::minstd_rand0& getEngine();

        static int randInt(int rangeStart, int rangeEnd);
        static long randLong(long rangeStart, long rangeEnd);
        static double randDouble(double rangeStart, double rangeEnd);

        static bool coinFlip();        

        static int randGaussianInt(int mu, int rangeStart, int rangeEnd);
        static long randGaussianLong(long mu, long rangeStart, long rangeEnd);
        static double randGaussianDouble(double mu, double rangestart,
                                         double rangeEnd);
};


#endif // _GGA_RAND_ENGINE_HPP_
