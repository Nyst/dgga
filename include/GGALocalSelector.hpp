//     This file is part of DGGA.
//     Copyright (C) 2015 Kevin Tierney and Josep Pon
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.


                
#ifndef _GGA_LOCAL_SELECTOR_HPP_
#define _GGA_LOCAL_SELECTOR_HPP_

#include <limits>

#include <boost/interprocess/sync/named_semaphore.hpp>
#include <boost/thread/locks.hpp>
#include <boost/thread/mutex.hpp>


#include "ggatypedefs.hpp"
#include "GGASelector.hpp"
#include "GGAGenome.hpp"
#include "GGARunner.hpp"


class GGALocalSelector : public GGASelector {
public:
    GGALocalSelector();
    virtual ~GGALocalSelector();

    void forceStop();
    void resetTimeout(double new_timeout);

    virtual GGASelectorResult select(const GGAGenomeVector& participants, 
                                     const GGAInstanceVector& instances,
                                     double timout = std::numeric_limits<double>::max());
private:
    // Intentionally unimplemented
    GGALocalSelector(const GGALocalSelector&);
    GGALocalSelector& operator=(const GGASelector&);

    void initializeTournament(const GGAGenomeVector& participants,
                              const GGAInstanceVector& instances,
                              double timeout);

    void registerInstancesResults(const RunnerVector& runners,
                                  const GGAInstanceVector& instances,
                                  GGAGenomeVector& genomes,
                                  GGASelectorResultBuilder& resultBuilder);
    
    void killRunners();
    void cleanUpRunners();
    void stopPoint();
           
    // attributes
    boost::mutex m_runners_mutex;
    RunnerVector m_runners;
    boost::interprocess::interprocess_semaphore m_runners_ctrl_semaphore;

    boost::mutex m_stop_mutex;
    bool m_stop_requested;

    const size_t m_num_threads;
    const bool m_runtime_tuning;
    const int m_cpu_limit;
    const double m_penalty;
    const double m_pct_winners;
    const bool m_propagate_timeout;
};

#endif // _GGA_LOCAL_SELECTOR_HPP_
