//     This file is part of DGGA.
//     Copyright (C) 2015 Kevin Tierney and Josep Pon
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.


                
/*
 * Class: OutputLog
 * Author: Kevin Tierney
 * Provides simple std out / std err logging capabilities
 */

#ifndef GA_PARAM_CONFIG_OUTPUT_LOG
#define GA_PARAM_CONFIG_OUTPUT_LOG

#include <iostream>
#include <sstream>
#include <string>

#include "ggatypedefs.hpp"

// TODO: Replace with something typesafe?
#define DO_LOG(text, level) {std::ostringstream oss; OutputLog::log((std::ostringstream&)(oss<<text), level);}
#define LOG_NOPRE(text) {std::ostringstream oss; OutputLog::log((std::ostringstream&)(oss<<text), OutputLog::NORMAL, false);}
#define LOG(text) DO_LOG(text, OutputLog::NORMAL)
#define LOG_ERROR(text) DO_LOG(text, OutputLog::ERROR)
#define LOG_VERBOSE(text) DO_LOG(text, OutputLog::VERBOSE)
#define LOG_VERY_VERBOSE(text) DO_LOG(text, OutputLog::VERY_VERBOSE)
#define LOG_DEBUG(text) DO_LOG(text, OutputLog::DEBUG)
#define LOG_ERROR_NOP(text) {std::ostringstream oss; OutputLog::log((std::ostringstream&)(oss<<text), OutputLog::ERROR, false);}

// TODO Thread safety!

class OutputLog {
public:
    
    enum Level {
        NOTHING         = 0,
        ERROR           = 1,
        NORMAL          = 2,
        VERBOSE         = 3,
        VERY_VERBOSE    = 4,
        DEBUG           = 5
    };
    
    OutputLog();
    virtual ~OutputLog();

    static void setReportLevel(Level lvl);
    static Level reportLevel();

    static void setMasterPid();
    static pid_t masterPid();
    
    static void log(const std::ostringstream& os, Level lvl, bool addPrefix = true);
    
    /*
     * Gets the logging output prefix formatted by:
     * [OUTPUT_LEVEL][CPU TIME] msg
     */
    static std::string prefix(Level lvl);

    static std::string genomeMapToString(GenomeMap g);
    static std::string stringVectorToString(const StringVector& sv, 
                                            const std::string& delim = ", ");
    static std::string intVectorToString(const IntVector& ivec,
                                         const std::string delim = ", ");
    static std::string uintVectorToString(const UIntVector& uivec,
                                          const std::string& delim = ", ");

    static Level s_ReportLevel;
    static pid_t s_masterPid;
};

#endif
