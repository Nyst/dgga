//     This file is part of DGGA.
//     Copyright (C) 2015 Kevin Tierney and Josep Pon
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.


                
#ifndef _GGA_POPULATION_HPP_
#define _GGA_POPULATION_HPP_

#include "GGAGenome.hpp"
#include "ggatypedefs.hpp"

class GGAPopulation {
public:
    GGAPopulation();
    virtual ~GGAPopulation();
    
    void addRandomGenomesWithGender(const GGAParameterTree& ptree, int howMany, GGAGenome::Gender gender);
    void addRandomGenomes(const GGAParameterTree& paramTree, int howMany);
    void addGenomes(const GGAGenomeVector& genomes);

    // Acces to population and its size
    const GGAGenomeVector& population(GGAGenome::Gender gender) const;
    GGAGenomeVector& population(GGAGenome::Gender gender);
    int populationSize() const;

    void addGenome(const GGAGenome& genome);

    void removeGenome(const GGAGenome& genome);

    void agePopulation();

    void updatePopulation(const GGAGenome&);
    void updatePopulation(const GGAGenomeVector&);
    
    std::string toString() const;
    
private:
    GGAGenomeVector m_popN;
    GGAGenomeVector m_popC;
};

//==============================================================================
// GGAPopulation public inline methods

/**
 *
 */
inline const GGAGenomeVector& GGAPopulation::population(
                                            GGAGenome::Gender gender) const
{
    if(gender == GGAGenome::COMPETITIVE) {
        return m_popC;
    } else {
        return m_popN;
    }
}

/**
 *
 */
inline GGAGenomeVector& GGAPopulation::population(GGAGenome::Gender gender)
{
    return const_cast<GGAGenomeVector&>(static_cast<const GGAPopulation*>(this)
                                                        ->population(gender));
}

/**
 *
 */
inline int GGAPopulation::populationSize() const
{
    return int(m_popC.size() + m_popN.size());
}

#endif
