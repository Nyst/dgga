//     This file is part of DGGA.
//     Copyright (C) 2015 Kevin Tierney and Josep Pon
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.


                
#ifndef _DGGA_NET_EXCEPTION_HPP_
#define _DGGA_NET_EXCEPTION_HPP_

#include <stdint.h>

#include "GGAExceptions.hpp"


class DGGANetException : public GGAException
{
public:
    //DGGANetException();
    DGGANetException(const DGGANetException&);
    DGGANetException(const std::string& error, const std::string& host);
    DGGANetException(const std::string& error, const std::string& host, 
                     uint16_t port);
};


#endif  // _DGGA_NET_EXCEPTION_HPP_

